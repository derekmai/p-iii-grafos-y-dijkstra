package grafo;

import java.io.Serializable;
import java.util.ArrayList;

public class Grafo implements Serializable
{
	private static final long serialVersionUID = 1L;
	private ArrayList<Nodo> vertices;
	private int[][] matrizDePesos;
	public Grafo (int cantidadVerticesInicial)
	{
		vertices = new ArrayList<Nodo>();
		for (int i = 0; i < cantidadVerticesInicial; i++)
			vertices.add(new Nodo(""+i,0));
		matrizDePesos = new int[cantidadVerticesInicial][cantidadVerticesInicial];
		// seteo todos los pesos de la matriz en infinito
		for (int i = 0; i < cantidadVerticesInicial; i++)
			for (int j = 0; j < cantidadVerticesInicial; j++)
				if (i != j)
					matrizDePesos[i][j] = Integer.MAX_VALUE;
	}
	
	public void agregarArista(int nodoOrigen, int nodoDestino,int peso)
	{
		chequearExistenciaNodos(nodoOrigen,nodoDestino);
		chequearQueNoExistaArista(nodoOrigen,nodoDestino);
		vertices.get(nodoOrigen).agregarVecino(vertices.get(nodoDestino));
		setPesoArista(nodoOrigen,nodoDestino,peso);
	}
	
	public void agregarArista(Nodo nodoOrigen, Nodo nodoDestino,int peso)
	{
		chequearExistenciaNodos(nodoOrigen,nodoDestino);
		chequearQueNoExistaArista(nodoOrigen,nodoDestino);
		nodoOrigen.agregarVecino(nodoDestino);
		setPesoArista(nodoOrigen,nodoDestino,peso);
	}
	private void setPesoArista(Nodo nodoOrigen, Nodo nodoDestino, int peso)
	{
			chequearIntegridadDelPeso(peso);
			matrizDePesos[vertices.indexOf(nodoOrigen)][vertices.indexOf(nodoDestino)] = peso;
	}

	private void chequearQueNoExistaArista(Nodo nodoOrigen, Nodo nodoDestino)
	{
		if (nodoOrigen.contiene(nodoDestino))
			throw new IllegalArgumentException("la arista ya existe!");
	}

	private void chequearExistenciaNodos(Nodo nodoOrigen, Nodo nodoDestino)
	{
		if (nodoOrigen.equals(nodoDestino))
			throw new IllegalArgumentException("El nodo de origen y el nodo destino tiene que ser distintos!");
		chequearQueExistaVertice(nodoOrigen.getNombre());
		chequearQueExistaVertice(nodoDestino.getNombre());
	}

	public void cambiarNombreNodo(int indiceNodo, String nuevoNombre)
	{
		
		chequearQueNoExistaVertice(nuevoNombre);
		chequearIndice(indiceNodo);
		vertices.get(indiceNodo).setNombre(nuevoNombre);
	}

	private void chequearIndice(int indiceNodo)
	{
		if (indiceNodo < 0 || indiceNodo >= vertices.size())
			throw new IllegalArgumentException("el indice es inv�lido. Indice: " + indiceNodo);
	}

	void chequearIntegridadDelPeso(int peso)
	{
		if (peso < 0)
			throw new IllegalArgumentException("El peso tiene que ser positivo!");
		if (peso == 0)
			throw new IllegalArgumentException("El peso tiene que ser mayor a 0!");
	}

	void chequearExistenciaNodos(int nodoOrigen, int nodoDestino)
	{
		if (nodoOrigen == nodoDestino)
			throw new IllegalArgumentException("Para crear una arista, el nodo de origen y el nodo destino tiene que ser distintos!");
		if (nodoOrigen < 0 || nodoOrigen >= vertices.size())
			throw new IllegalArgumentException("El nodo de Origen tiene un indice inexistente. Indice: "+ nodoOrigen);

		if (nodoDestino < 0 || nodoDestino >= vertices.size())
			throw new IllegalArgumentException("El nodo destino tiene un indice inexistente. Indice: "+ nodoDestino);
	}

	void chequearQueNoExistaArista(int nodoOrigen, int nodoDestino)
	{
		if (vertices.get(nodoOrigen).contiene(vertices.get(nodoDestino)))
			throw new IllegalArgumentException("la arista ya existe!");
	}

	void chequearQueExistaArista(int nodoOrigen, int nodoDestino)
	{
		if (!vertices.get(nodoOrigen).contiene(vertices.get(nodoDestino)))
			throw new IllegalArgumentException("La arista no existe!");
	}
	
	public void agregarVertice(String nombre,Integer cantidadCarbon)
	{
		chequearQueNoExistaVertice(nombre);
		
		vertices.add(new Nodo(nombre,cantidadCarbon));
		redefinirMatrizDePesos();
		
	}
	public void agregarVertice(Nodo nuevoVertice)
	{
		System.out.println("entre");
		chequearQueNoExistaVertice(nuevoVertice.getNombre());
		vertices.add(nuevoVertice);
		redefinirMatrizDePesos();
	}
	
	void chequearQueNoExistaVertice(String nombre)
	{
		for (Nodo nodo : vertices)
			if (nodo.getNombre().equals(nombre))
				throw new IllegalArgumentException("Ya existe un nodo con ese nombre!");
	}
	
	private void redefinirMatrizDePesos()
	{
		int antiguaCantidadDeVertices = matrizDePesos.length;
		int[][] auxiliar = new int[antiguaCantidadDeVertices+1][antiguaCantidadDeVertices+1];
		for(int i = 0; i < antiguaCantidadDeVertices; i++)
			for (int j = 0; j < antiguaCantidadDeVertices; j++)
				auxiliar[i][j] = matrizDePesos[i][j];
		
		for (int j = 0; j < antiguaCantidadDeVertices; j++)
			if (j != antiguaCantidadDeVertices)
				auxiliar[antiguaCantidadDeVertices][j] = Integer.MAX_VALUE;
		matrizDePesos = auxiliar;
	}
	
	public void chequearQueExistaVertice(String nombre)
	{
		for (Nodo nodo : vertices)
			if (nodo.getNombre().equals(nombre))
				return;
		throw new IllegalArgumentException("Ya existe un nodo con ese nombre!");
	}

	public Nodo getVertice(int i)
	{
		return vertices.get(i);
	}
	
	public Nodo getVertice(String nombre)
	{
		for(Nodo n : this.vertices)
		{
			if(n.getNombre().equalsIgnoreCase(nombre))
			{
				return n;
			}
		}
		return null;
	}
	
	public int getCantidadVertices()
	{
		return vertices.size();
	}
	
	public int getPesoArista(int origen, int destino)
	{
		chequearQueExistaArista(origen,destino);
		return matrizDePesos[origen][destino];
	}
	
	public void setPesoArista(int origen, int destino, int peso)
	{
		chequearQueExistaArista(origen,destino);
		chequearIntegridadDelPeso(peso);
		matrizDePesos[origen][destino] = peso;
	}

	public int[][] getMatrizDePesos()
	{
		return this.matrizDePesos.clone();
	}

	public int indexOf(Nodo verticeInicial)
	{
		return vertices.indexOf(verticeInicial);
	}

	public ArrayList<Nodo> getVertices()
	{
		return this.vertices;
	}
 }