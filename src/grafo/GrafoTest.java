package grafo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GrafoTest
{
	@Before
	public void inicializar()
	{
		grafo = new Grafo(4);
	}
	
	private Grafo grafo;
	
	
	@Test
	public void agregarAristaYAgregarVerticeEnMatrizTest()
	{
		grafo.agregarArista(0, 3, 5);
		grafo.agregarVertice("Test", 5);
		
		assertEquals(5,grafo.getPesoArista(0, 3));
	}
	@Test
	public void agregarVerticeEnGrafoTest()
	{
		grafo.agregarVertice("Test", 5);
		
		assertEquals("Test",grafo.getVertice(4).getNombre());
	}
	
	@Test
	public void agregarAristaTest()
	{
		grafo.agregarArista(0, 1, 5);
		
		assertTrue(grafo.getVertice(0).contiene(grafo.getVertice(1)));
	}
	
	@Test
	public void chequearPesoAristaTest()
	{
		grafo.agregarArista(0, 1, 5);
		
		assertEquals(5,grafo.getPesoArista(0, 1));
	}
	
	@Test
	public void chequearPesoAristaCambiadoTest()
	{
		grafo.agregarArista(2, 3, 5);
		grafo.setPesoArista(2, 3, 10);
		assertEquals(10,grafo.getPesoArista(2, 3));
	}
	
	
	@Test (expected = IllegalArgumentException.class)
	public void chequearPesoAristaDeUnMismoVerticeTest()
	{
		grafo.getPesoArista(0,0);
	}
	
	
	@Test
	public void AgregarAristaExtremoTest()
	{
		grafo.agregarArista(0, 3, 5);
		
		assertTrue(grafo.getVertice(0).contiene(grafo.getVertice(3)));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaConDestinoInexistenteTest()
	{
		grafo.agregarArista(0, 4, 5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaConOrigenInexistenteTest()
	{
		grafo.agregarArista(-1, 3, 5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaConOrigenYDestinoInexistenteTest()
	{
		grafo.agregarArista(-1, 4, 5);
	}
	
	@Test
	public void chequearAristaTest()
	{
		grafo.chequearQueNoExistaArista(0,1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void chequearAristaExistenteTest()
	{
		grafo.agregarArista(0, 2, 5);
		grafo.chequearQueNoExistaArista(0,2);
	}
	
	@Test
	public void chequearExistenciaTest()
	{
		grafo.chequearExistenciaNodos(2,1);
	}
	@Test
	public void chequearExistenciaEnExtremosTest()
	{
		grafo.chequearExistenciaNodos(0,3);
	}
	@Test
	public void chequearExistenciaEnDestinoExtremoTest()
	{
		grafo.chequearExistenciaNodos(2,3);
	}
	@Test
	public void chequearExistenciaEnOrigenExtremoTest()
	{
		grafo.chequearExistenciaNodos(0,2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void chequearExistenciaconDestinoExcedidoTest()
	{
		grafo.chequearExistenciaNodos(0,4);
	}
	@Test(expected = IllegalArgumentException.class)
	public void chequearExistenciaconOrigenNegativoTest()
	{
		grafo.chequearExistenciaNodos(-1,3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void chequearPesoNegativoTest()
	{
		grafo.chequearIntegridadDelPeso(-1);
	}
	
}
