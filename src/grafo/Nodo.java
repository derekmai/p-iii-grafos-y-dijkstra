package grafo;

import java.io.Serializable;
import java.util.HashSet;

public class Nodo implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String nombre;
	private Integer cantidadCarbon;
	private HashSet<Nodo> vecinos;
	
	
	public Nodo(String nombre, Integer cantidadCarbon)
	{
		this.setNombre(nombre);
		this.setCantidadCarbon(cantidadCarbon);
		vecinos = new HashSet<>();
	}

	public String getNombre()
	{
		return nombre;
	}

	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}

	public Integer getCantidadCarbon()
	{
		return cantidadCarbon;
	}

	public void setCantidadCarbon(Integer cantidadCarbon)
	{
		chequearNuevoValor(cantidadCarbon);
		this.cantidadCarbon = cantidadCarbon;
	}

	private void chequearNuevoValor(Integer cantidadCarbon)
	{
		if (cantidadCarbon < 0)
			throw new IllegalArgumentException("la cantidad de carbon en un nodo debe ser mayor o igual a 0. Usted ingres� "+cantidadCarbon);
	}

	void agregarVecino(Nodo nodo)
	{
		vecinos.add(nodo);
	}

	boolean contiene(Nodo nodo)
	{
		// Si el hashSet esta bien distribuido, O(1).
		return this.vecinos.contains(nodo);
	}

	public HashSet<Nodo> getVecinos()
    {
		return vecinos;
	}

	public boolean tieneCarbon()
	{
		return cantidadCarbon >0;
	}

	
	
}
