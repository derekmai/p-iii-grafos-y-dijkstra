package dijkstra;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

import grafo.Grafo;
import grafo.Nodo;

public class DijkstraTest
{

	@Before
	public void Inicializar()
	{
		grafo = new Grafo (6);
		/*grafo.agregarVertice("1", 0);
		grafo.agregarVertice("2", 0);
		grafo.agregarVertice("3", 0);
		grafo.agregarVertice("4", 0);
		grafo.agregarVertice("5", 0);
		grafo.agregarVertice("6", 0);*/
		
		grafo.agregarArista(0, 1, 4);
		grafo.agregarArista(0, 5, 3);
		grafo.agregarArista(0, 2, 8);
		grafo.agregarArista(1, 2, 3);
		grafo.agregarArista(1, 4, 1);
		grafo.agregarArista(2, 3, 1);
		grafo.agregarArista(2, 4, 1);
		grafo.agregarArista(4, 3, 4);
		grafo.agregarArista(5, 4, 3);
		for (Nodo vertice: grafo.getVertices())
			vertice.setCantidadCarbon(1);
		solver = new Solver(grafo);
		String []auxiliar = solver.getCaminosACadaNodo();
		for (int i = 0; i<auxiliar.length;i++)
				auxiliar[i] = "";
	}
	
	private Grafo grafo;
	private Solver solver;
	@Test
	public void recorridaCompletaTest()
	{
		solver.dijkstra(grafo.getVertice(0));
		int [] expected = {0,4,7,8,5,3};
		boolean resultadoExitoso = assertArregloEquals(expected);
		assertTrue(resultadoExitoso);
	}

	@Test
	public void recorridaParcialTest()
	{

		solver.dijkstra(grafo.getVertice(2));
		int [] expected = {Integer.MAX_VALUE,Integer.MAX_VALUE,0,1,1,Integer.MAX_VALUE};
		boolean resultadoExitoso = assertArregloEquals(expected);
		assertTrue(resultadoExitoso);
	}
	@Test
	public void SinCaminoTest()
	{

		solver.dijkstra(grafo.getVertice(3));
		int [] expected = {Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE,0,Integer.MAX_VALUE,Integer.MAX_VALUE};
		boolean resultadoExitoso = assertArregloEquals(expected);
		assertTrue(resultadoExitoso);
	}
	@Test
	public void unSoloCaminoTest()
	{

		solver.dijkstra(grafo.getVertice(4));
		int [] expected = {Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE,4,0,Integer.MAX_VALUE};
		boolean resultadoExitoso = assertArregloEquals(expected);
		assertTrue(resultadoExitoso);
	}
	@Test
	public void alcanzablesLineaRectaTest()
	{

		solver.dijkstra(grafo.getVertice(5));
		int [] expected = {Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE,7,3,0};
		boolean resultadoExitoso = assertArregloEquals(expected);
		assertTrue(resultadoExitoso);
	}
	
	@Test
	public void recorridaCompletaNodoCarbonTest()
	{

		Nodo test = solver.dameNodoConCarbonMasCercano(grafo.getVertice(0));
		Nodo expected = grafo.getVertice(5);
		boolean resultadoExitoso = assertNodoEquals(expected, test);
		assertTrue(resultadoExitoso);
	}
	
	@Test
	public void recorridaCompletaSinCarbonTest()
	{

		for (Nodo vertice: grafo.getVertices())
			vertice.setCantidadCarbon(0);
		grafo.getVertice(3).setCantidadCarbon(1);
		
		Nodo test = solver.dameNodoConCarbonMasCercano(grafo.getVertice(0));
		Nodo expected = grafo.getVertice(3);
		boolean resultadoExitoso = assertNodoEquals(expected, test);
		assertTrue(resultadoExitoso);
	}

	@Test(expected = IllegalArgumentException.class)
	public void recorridaCompletaTodosSinCarbonTest()
	{
		for (Nodo vertice: grafo.getVertices())
			vertice.setCantidadCarbon(0);
		
		solver.dameNodoConCarbonMasCercano(grafo.getVertice(0));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void recorridaParcialSinCarbonEnCaminoTest()
	{
		grafo.getVertice(2).setCantidadCarbon(0);
		grafo.getVertice(3).setCantidadCarbon(0);
		grafo.getVertice(4).setCantidadCarbon(0);
		solver.dameNodoConCarbonMasCercano(grafo.getVertice(2));
	}
	
	@Test
	public void recorridaParcialNodoCarbonTest()
	{

		Nodo test = solver.dameNodoConCarbonMasCercano(grafo.getVertice(2));
		Nodo expected = grafo.getVertice(3);
		boolean resultadoExitoso = assertNodoEquals(expected, test);
		assertTrue(resultadoExitoso);
	}
	
	@Test
	public void recorridaParcialNodoCarbonSinCarbonTest()
	{
		grafo.getVertice(3).setCantidadCarbon(0);
		Nodo test = solver.dameNodoConCarbonMasCercano(grafo.getVertice(2));
		Nodo expected = grafo.getVertice(4);
		boolean resultadoExitoso = assertNodoEquals(expected, test);
		assertTrue(resultadoExitoso);
	}


	@Test(expected = IllegalArgumentException.class)
	public void SinCaminoNodoCarbonTest()
	{

		solver.dameNodoConCarbonMasCercano(grafo.getVertice(3));
		
	}
	
	@Test
	public void unSoloCaminoNodoCarbonTest()
	{

		Nodo test = solver.dameNodoConCarbonMasCercano(grafo.getVertice(2));
		Nodo expected = grafo.getVertice(3);
		boolean resultadoExitoso = assertNodoEquals(expected, test);
		assertTrue(resultadoExitoso);
	}
	public void alcanzablesLineaRectaNodoCarbonTest()
	{

		Nodo test = solver.dameNodoConCarbonMasCercano(grafo.getVertice(5));
		Nodo expected = grafo.getVertice(4);
		boolean resultadoExitoso = assertNodoEquals(expected, test);
		assertTrue(resultadoExitoso);
	}

	
	private boolean assertArregloEquals(int[] expected)
	{
		boolean ret = true;
		if (expected.length != solver.getDistanciasMinimasAcadaNodo().length)
			return !ret;
		for (int i = 0; i < expected.length; i++)
			ret = ret && expected[i] == solver.getDistanciasMinimasAcadaNodo()[i];
		return ret;
	}
	private boolean assertNodoEquals(Nodo expected, Nodo test)
	{
		return expected.equals(test);
	}

}
