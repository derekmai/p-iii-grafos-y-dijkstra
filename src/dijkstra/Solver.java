package dijkstra;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import grafo.Grafo;
import grafo.Nodo;
import interfaz.AristaFigura;

public class Solver
{
	private Grafo grafo;
	private String[]caminosACadaNodo;
	private int[]distanciasMinimasAcadaNodo;
	private boolean solucionGenerada =false;
	private int index = -1;
	

	public Solver(Grafo grafo)
	{
		this.grafo = grafo;
		int cant = grafo.getCantidadVertices();
		caminosACadaNodo = new String[cant];
		for (int i = 0 ; i < caminosACadaNodo.length ; i++)
			caminosACadaNodo[i] = "";
		distanciasMinimasAcadaNodo = new int[cant];
	}
	public Nodo dameNodoConCarbonMasCercano(Nodo verticeOrigen)
	{
		index = -1;
		solucionGenerada = false;
		dijkstra(verticeOrigen);
		index = indiceDeNodoConCarbonMasCercano();
		agregarUltimoDestinoParaLlegarANodo();
		chequearQueExistaIndice(verticeOrigen, index);
		System.out.println(grafo.getVertice(index).getNombre());
		solucionGenerada = true;
		System.out.println(caminosACadaNodo[index]);
		return grafo.getVertice(index);
		
	}
	private void agregarUltimoDestinoParaLlegarANodo() {
		for (int i = 0; i < caminosACadaNodo.length;i++)
			caminosACadaNodo[i] += i;
	}

	private int indiceDeNodoConCarbonMasCercano()
	{
		int index = -1;
		int distanciaMasCortaANodo = Integer.MAX_VALUE;
		for (int i = 0; i < distanciasMinimasAcadaNodo.length;i++)
			if (distanciasMinimasAcadaNodo[i] != 0) //es el nodo origen.
				if (grafo.getVertice(i).tieneCarbon() && distanciasMinimasAcadaNodo[i]<distanciaMasCortaANodo)
				{
					index = i;
					distanciaMasCortaANodo =  distanciasMinimasAcadaNodo[i];
				}
		return index;
	}
	
	public void dijkstra(Nodo verticeOrigen)
	{
		grafo.chequearQueExistaVertice(verticeOrigen.getNombre());	
		inicializarArregloDeDistancias(verticeOrigen);
		
		int cantidadVertices = grafo.getCantidadVertices();
		boolean[] visitados = new boolean[cantidadVertices];
		int iteracion = 0;
		while (iteracion < cantidadVertices)	
		{
			int indice = IndiceDeMenorDistancia(distanciasMinimasAcadaNodo,visitados);
			if (indice == -1)
			{
				int indexOrigen = grafo.indexOf(verticeOrigen);
				caminosACadaNodo[indexOrigen] = "Origen";
				return;
			}
			Nodo nodoActual = grafo.getVertice(indice);
			visitados[grafo.indexOf(nodoActual)] = true;
			for (Nodo vecino : nodoActual.getVecinos())
			{
				int indexDelVecino = grafo.indexOf(vecino), indexDelNodoActual = grafo.indexOf(nodoActual);
				int calculoDistancia = grafo.getPesoArista(indexDelNodoActual, indexDelVecino) + distanciasMinimasAcadaNodo[indexDelNodoActual];
				if (calculoDistancia <= distanciasMinimasAcadaNodo[indexDelVecino])
				{
					distanciasMinimasAcadaNodo[indexDelVecino] = calculoDistancia;
					caminosACadaNodo[indexDelVecino] = caminosACadaNodo[indexDelNodoActual] + indexDelNodoActual+' ';
				}
			}
			iteracion++;
		}
		int indexOrigen = grafo.indexOf(verticeOrigen);
		caminosACadaNodo[indexOrigen] = "Origen";
	}
	
	private void chequearQueExistaIndice(Nodo verticeOrigen, int index)
	{
		if (index == -1)
			throw new IllegalArgumentException("se quiso buscar un nodo con carbon cercano desde el Nodo "+ verticeOrigen.getNombre() + ", pero este no tiene camino posible a un nodo con carbon!");
	}
	
	private void inicializarArregloDeDistancias(Nodo verticeOrigen)
	{
		int cantidadVertices = grafo.getCantidadVertices();
		
		distanciasMinimasAcadaNodo = new int[cantidadVertices];
		
		// si no son el vertice inicial, arrancan en infinito.
		for (int i = 0; i < cantidadVertices; i++)
			if (!grafo.getVertice(i).equals(verticeOrigen))
				distanciasMinimasAcadaNodo[i] = Integer.MAX_VALUE;
	}
	
	private static int IndiceDeMenorDistancia(int[] arreglo, boolean[] visitados)
	{
		int ret = -1;
		int minimo = Integer.MAX_VALUE;
		for (int i = 0; i < arreglo.length; i ++)
			if (arreglo[i] < minimo && visitados[i] == false)
			{
				ret = i;
				minimo = arreglo[i];
			}
		return ret;
	}
	public int[] getDistanciasMinimasAcadaNodo()
	{
		return distanciasMinimasAcadaNodo;
	}
	public String[]getCaminosACadaNodo()
	{
		return this.caminosACadaNodo;
	}
	public boolean aristaEnCaminoMinimo(AristaFigura t)
	{
		boolean ret = false;
		String dondeBuscar = caminosACadaNodo[index];
		if (haySolucion())
		{
			ArrayList<Nodo> nodos = grafo.getVertices();
			for (int i = 0 ; i <caminosACadaNodo.length;i++)
			{
				String aBuscar = "" + nodos.indexOf(grafo.getVertice(t.getNombreOrigen()));
				aBuscar = aBuscar + " " +   nodos.indexOf(grafo.getVertice(t.getNombreDestino()));
				ret = ret || perteneceANodo(dondeBuscar,aBuscar);
			}
		}
		return ret;
	}
	private boolean perteneceANodo(String dondeBuscar, String aBuscar) 
	{
		boolean ret = false;
		if (dondeBuscar != null)
		{
			String aux = aBuscar;
			Pattern patron = Pattern.compile(aux);
			Matcher matcher = patron.matcher(dondeBuscar);
			if (matcher.find())
				ret = true;
		}
		return ret;
	}
	
	public boolean haySolucion()
	{
		return solucionGenerada;
	}
	
	public int getIndex()
	{
		return this.index;
	}
}
