package interfaz;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;

import grafo.Grafo;
import model.Controlador;

public class MainApp extends JFrame implements ActionListener
{
	
	private static final long serialVersionUID = 1L;
	Controlador miControlador;

	public MainApp(Controlador controlador)
	{
		this.miControlador = controlador;
		this.configurarMain();
		this.crearPanelInicio();
	}
	
	private void configurarMain()
	{	
		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension resolucion = miPantalla.getScreenSize();
		
		int anchoPantalla = (int) resolucion.getWidth();
		int altoPantalla = (int) resolucion.getHeight();
		
		this.setSize(anchoPantalla/2, altoPantalla/2);
		this.setLocation(anchoPantalla/4, altoPantalla/4);
		
		this.miControlador.setVentana(this);
		this.setTitle("Mi mina");
		this.setResizable(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public Grafo getGrafoArmado()
	{
		return this.panelVisualizador.parsearGrafo();
	}
	
	public String getNombreMapaNuevo()
	{
		return this.panelMapaNuevo.getNombreMapa();
	}
	
	public int getMapaSeleccionado() 
	{
		return this.panelCarga.getOpcionSeleccionada();
	}
	
	public ArrayList<NodoFigura> getNodosFigura() 
	{
		return this.panelVisualizador.getPuntosDeInteres();
	}
	
	public ArrayList<AristaFigura> getAristasFigura() 
	{
		return this.panelVisualizador.getTuneles();
	}
	
	public void setFiguras(ArrayList<NodoFigura> puntosDeInteres, ArrayList<AristaFigura> tuneles)
	{
		this.panelVisualizador.setDibujo(puntosDeInteres,tuneles);
	}
	
	public boolean estaHabilitadoAgregarPunto()
	{
		return this.panelVisualizador.sePuedeAgregarPunto();
	}
	public boolean estaHabilitadoAgregarTunel()
	{
		return this.panelVisualizador.sePuedeAgregarTunel();
	}
	public void habilitarAgregarPunto()
	{
		this.panelVisualizador.habilitarAgregarPunto();
	}
	
	public void deshabilitarAgregarPunto()
	{
		this.panelVisualizador.deshabilitarAgregarPunto();
	}
	public void habilitarAgregarTunel()
	{
		this.panelVisualizador.habilitarAgregarTunel();
	}
	public void deshabilitarAgregarTunel()
	{
		this.panelVisualizador.deshabilitarAgregarTunel();
	}
	public void botonCaminoMinimo(boolean b) 
	{
		this.btnCaminoMinimo.setEnabled(b);
	}
	
	public void botonCrearTunel(boolean b)
	{
		this.btnAgregarTunel.setEnabled(true);
	}
	public void resetearOrigenYDestinoTunel()
	{
		this.panelVisualizador.resetearOrigenYDestinoTunel();
	}

	
	public void crearPanelInicio()
	{
		this.getContentPane().removeAll();
		this.panelInicio = new PanelInicio();
		this.panelInicio.add(this.crearBoton("Crear mapa", 10, 10, 150, 20));
		this.panelInicio.add(this.crearBoton("Cargar mapa", 10, 40, 150, 20));
		this.getContentPane().add(panelInicio);
		this.setVisible(true);
	}
	
	private PanelInicio panelInicio;
	
	public void crearPanelNuevoMapa()
	{
		this.getContentPane().removeAll();
		this.panelMapaNuevo = new PanelMapaNuevo();
		this.panelMapaNuevo.add(this.crearBoton("Crear", 77, 60, 67, 20));
		this.panelMapaNuevo.add(this.crearBoton("Volver al inicio", 241, 30, 200, 20));
		this.getContentPane().add(panelMapaNuevo);
		this.setVisible(true);
	}
	
	private PanelMapaNuevo panelMapaNuevo;
	
	public void crearPanelCarga(String[] opciones)
	{
		this.getContentPane().removeAll();
		this.panelCarga = new PanelCargaMapa(opciones);
		this.panelCarga.add(this.crearBoton("Cargar", 10, 50, 150, 20));
		this.panelCarga.add(this.crearBoton("Volver al inicio", 10, 80, 150, 20));
		this.getContentPane().add(panelCarga);
		this.setVisible(true);
	}
	
	private PanelCargaMapa panelCarga;
	
	public void crearPanelVisualizacion()
	{
		this.getContentPane().removeAll();
		this.panelVisualizador = new PanelVisualizador();
		this.panelVisualizador.add(this.crearBoton("Volver al inicio", 10, 10, 120, 20));
		this.panelVisualizador.add(this.crearBoton("Guardar", 10, 50, 120, 20));
		
		this.btnCaminoMinimo = this.crearBoton("Aplicar camino minimo", 160, 10, 170, 20);
		this.btnCaminoMinimo.setEnabled(false);
		this.btnCaminoMinimo.setBackground(new Color(255, 215, 0));
		this.panelVisualizador.add(this.btnCaminoMinimo);
		
		this.btnAgregarPunto = this.crearBoton("Agregar punto", 10, 90, 120, 20);
		this.panelVisualizador.add(btnAgregarPunto);
		this.btnAgregarTunel = this.crearBoton("Agregar tunel", 10, 130, 120, 20);
		this.panelVisualizador.add(btnAgregarTunel);
		this.getContentPane().add(panelVisualizador);
		this.setVisible(true);
	}
	
	private JButton btnAgregarPunto;
	private JButton btnAgregarTunel;
	private JButton btnCaminoMinimo;
	private PanelVisualizador panelVisualizador;
	
	private JButton crearBoton(String titulo, int x, int y, int ancho, int alto) 
	{
		JButton btn = new JButton(titulo);
		btn.setBounds(x, y, ancho, alto);
		btn.addActionListener(this);
		return btn;
	}

	public static void main(String[] args) throws FileNotFoundException, IOException 
	{
		MainApp ventana = new MainApp(new Controlador());
		ventana.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		this.miControlador.actionPerformed(e);
	}

	public void setGrafoAux(Grafo grafo)
	{
		this.panelVisualizador.setGrafoAux(grafo);
	}

	public void habilitarSePuedeCaminoMinimo()
	{
		this.panelVisualizador.setSePuedeAplicarCaminoMinimo(true);
	}

	public void deshabilitarSePuedeCaminoMinimo()
	{
		this.panelVisualizador.setSePuedeAplicarCaminoMinimo(false);
	}

	public boolean estaHabilitadoCaminoMinimo()
	{
		return this.panelVisualizador.sePuedeAplicarCaminoMinimo();
	}

	public void setSolver(Object object)
	{
		this.panelVisualizador.setSolver(null);
	}

}
