package interfaz;

import java.awt.Color;

import javax.swing.JPanel;

public class PanelInicio extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	public PanelInicio()
	{
		this.setBackground(new Color(62,62,64));
		this.setLayout(null);
		this.setVisible(true);
	}
}
