package interfaz;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.Serializable;

import grafo.Nodo;

public class NodoFigura implements Serializable
{
	private static final long serialVersionUID = 1L;
	private int x,y;
	public static final int diametro = 15;
	private Nodo nodo;
	
	public NodoFigura(String nombre, int carbonReserva, int x, int y) 
	{
		nodo = new Nodo(nombre,carbonReserva);
		this.x = x;
		this.y = y;
	}

	public void pintar(Graphics g)
	{
		g.setColor(Color.WHITE);
		g.fillOval(this.x - NodoFigura.diametro/2, this.y - NodoFigura.diametro/2, NodoFigura.diametro, NodoFigura.diametro);
		pintarNombre(g);
		pintarReservaCarbon(g);
	}

	public void pintarDestino(Graphics g)
	{
		g.setColor(Color.GREEN);
		g.fillOval(this.x - NodoFigura.diametro/2, this.y - NodoFigura.diametro/2, NodoFigura.diametro, NodoFigura.diametro);
		pintarNombre(g);
		pintarReservaCarbon(g);
	} 
	private void pintarNombre(Graphics g)
	{
		g.setColor(Color.BLACK);
		g.setFont(new Font(this.nodo.getNombre(), Font.TYPE1_FONT, 15));
		g.drawString(this.nodo.getNombre(), x-15, y+20);
	}
	
	private void pintarReservaCarbon(Graphics g) 
	{
		if(this.nodo.getCantidadCarbon() != 0)
		{
			g.setColor(Color.BLACK);
			g.setFont(new Font("carbon: "+this.nodo.getCantidadCarbon()+" Tn", Font.BOLD, 12));
			g.drawString("carbon: "+this.nodo.getCantidadCarbon()+" Tn", x-15, y+35);
		}
		else
		{
			g.setColor(Color.RED);
			g.setFont(new Font("carbon: "+this.nodo.getCantidadCarbon()+" Tn", Font.BOLD, 12));
			g.drawString("carbon: "+this.nodo.getCantidadCarbon()+" Tn", x-15, y+35);
		}
	}
	
	public int getReservaCarbon() 
	{
		return nodo.getCantidadCarbon();
	}

	public void setReservaCarbon(int reservaCarbon) 
	{
		this.nodo.setCantidadCarbon(reservaCarbon);
	}

	public String getNombre()
	{
		return this.nodo.getNombre();
	}

	public void setNombre(String nombre)
	{
		this.nodo.setNombre(nombre);
	}
	
	public int getX() 
	{
		return x;
	}

	public void setX(int x) 
	{
		this.x = x;
	}

	public int getY() 
	{
		return y;
	}

	public void setY(int y) 
	{
		this.y = y;
	}

}
