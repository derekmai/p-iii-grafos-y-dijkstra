package interfaz;

import java.awt.Color;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;

public class PanelCargaMapa extends JPanel
{
	private static final long serialVersionUID = 1L;
	private JComboBox<String> barraCarga;

	public PanelCargaMapa(String[] opciones)
	{
		this.setBackground(new Color(62,62,64));
		this.setLayout(null);
		this.configurarBarra(opciones);
		this.setVisible(true);
	}
	
	private void configurarBarra(String[] opciones) 
	{
		this.barraCarga = new JComboBox<String>();
		this.barraCarga.setModel(new DefaultComboBoxModel<String>(opciones));
		this.barraCarga.setBounds(10, 10, 200, 20);
		this.add(barraCarga);
	}

	public int getOpcionSeleccionada()
	{
		return this.barraCarga.getSelectedIndex();
	}
}
