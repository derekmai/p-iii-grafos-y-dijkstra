package interfaz;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PanelMapaNuevo extends JPanel
{
	private static final long serialVersionUID = 1L;
	JTextField input;
	JLabel etiqueta;
	
	public PanelMapaNuevo()
	{
		this.setLayout(null);
		this.setBackground(new Color(62,62,64));
		this.crearComponentes();
		
		this.setVisible(true);
	}
	
	private void crearComponentes() 
	{ 	
		this.input = new JTextField();
		this.input.setBounds(10, 30, 201, 20);
		this.add(input);
		
		this.etiqueta = new JLabel("Ingrese el nombre del nuevo mapa");
		this.etiqueta.setBounds(10, 10, 200, 20);
		this.etiqueta.setForeground(Color.WHITE);
		this.add(this.etiqueta);
	}

	public String getNombreMapa() 
	{
		if(input.getText() != null)
		{
			return input.getText();
		}
		return "";
	}
	
	
}
