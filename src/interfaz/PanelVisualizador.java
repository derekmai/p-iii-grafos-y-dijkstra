package interfaz;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import dijkstra.Solver;
import grafo.Grafo;
import grafo.Nodo;

public class PanelVisualizador extends JPanel implements MouseListener, MouseMotionListener
{
	private static final long serialVersionUID = 1L;
	private GrafoDibujo grafoDibujo;
	private Solver solver = null;
	
	PanelVisualizador()
	{
		this.configurarPanel();
	}
	
	private void configurarPanel() 
	{
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.grafoDibujo = new GrafoDibujo();
		this.sePuedeAgregarPunto = false;
		this.sePuedeAgregarTunel = false;
		this.punteroMouse = new Point();
		
		this.setBackground(new Color(105,105,105));
		this.setLayout(null);
		this.setVisible(true);
	}
	
//*****************************************************METODOS PUBLICOS
	
	@Override
	public void mouseClicked(MouseEvent evento) 
	{
		if(sePuedeAgregarPunto)
		{
			if(evento.getButton() == MouseEvent.BUTTON1)
			{
				for(NodoFigura nodo : this.grafoDibujo.getPuntosDeInteres())
				{
					if(this.grafoDibujo.chequearClickEnAreaNodo(nodo, evento))
					{
						JOptionPane.showMessageDialog(this,"Ya hay un nodo en ese lugar!");
						return;
					}
				}
				try
				{
					this.grafoDibujo.crearNodo(evento);
				}
				catch (IllegalArgumentException e)
				{
					this.sePuedeAgregarPunto = false;
					JOptionPane.showMessageDialog(this, e.getMessage());
					e.printStackTrace();
				}
			}
			this.sePuedeAgregarPunto = false;
		}
		if(sePuedeAgregarTunel)
		{
			if(evento.getButton() == MouseEvent.BUTTON1)
			{
				for(NodoFigura nodo : this.grafoDibujo.getPuntosDeInteres())
				{
					if(this.grafoDibujo.chequearClickEnAreaNodo(nodo, evento))
					{
						this.repaint();
						try
						{
							this.grafoDibujo.fijarExtremosXY(nodo);
						}
						catch (IllegalArgumentException e)
						{
							JOptionPane.showMessageDialog(this, e.getMessage());
							this.sePuedeAgregarTunel = false;
							this.grafoDibujo.resetearOrigenYDestinoTunel();
							e.printStackTrace();
						}
					}
				}
				if(this.grafoDibujo.OcupacionPuntoOrigen() == false && this.grafoDibujo.OcupacionPuntoDestino() == false)
					this.sePuedeAgregarTunel = false;
			}
		}
		if(sePuedeAplicarCaminoMinimo)
		{
			if(evento.getButton() == MouseEvent.BUTTON1)
			{
				for(NodoFigura nodo :  this.grafoDibujo.getPuntosDeInteres())
				{
					if(this.grafoDibujo.chequearClickEnAreaNodo(nodo, evento))
					{
						try
						{
							Grafo auxiliar = grafoDibujo.getGrafo();
							solver = new Solver(auxiliar);
							Nodo nodoAux = auxiliar.getVertice(nodo.getNombre());
							solver.dameNodoConCarbonMasCercano(nodoAux);
							this.repaint();							
							this.sePuedeAplicarCaminoMinimo = false;
							return;
						}
						catch(IllegalArgumentException e)
						{
							JOptionPane.showMessageDialog(this, e.getMessage());
							this.sePuedeAplicarCaminoMinimo = false;
							e.printStackTrace();
						}
					}
				}
			}
		}
		this.repaint();
	}
	
	private boolean sePuedeAplicarCaminoMinimo;
	public boolean sePuedeAplicarCaminoMinimo()
	{
		return sePuedeAplicarCaminoMinimo;
	}

	public void setSePuedeAplicarCaminoMinimo(boolean sePuedeAplicarCaminoMinimo)
	{
		this.sePuedeAplicarCaminoMinimo = sePuedeAplicarCaminoMinimo;
	}

	private boolean sePuedeAgregarPunto;
	private boolean sePuedeAgregarTunel;
	
	public boolean sePuedeAgregarPunto()
	{
		return sePuedeAgregarPunto;
	}
	public boolean sePuedeAgregarTunel()
	{
		return sePuedeAgregarTunel;
	}
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		pintarIndicadorDeAgregarTunel(g);
		pintarPunteroDeAgregarPunto(g);
		
		for(AristaFigura t : this.grafoDibujo.getTuneles())
		{
			if (solver != null)
				if (solver.haySolucion() && solver.aristaEnCaminoMinimo(t))
					t.pintarCamino(g);
				else
					t.pintar(g);
			else
				t.pintar(g);
		}
		
		for(NodoFigura n : this.grafoDibujo.getPuntosDeInteres())
		{
			if (solver !=null)
			{
				if (solver.haySolucion())
				{
					if (this.grafoDibujo.getPuntosDeInteres().indexOf(n) == solver.getIndex())
					{		
						n.pintarDestino(g);
					}
					else
						n.pintar(g);
				}
				else
					n.pintar(g);
			}
			else
					n.pintar(g);
		}
	}
	
	public Grafo parsearGrafo()
	{
		return this.grafoDibujo.getGrafo();
	}
	
	public void setDibujo(ArrayList<NodoFigura> puntosDeInteres, ArrayList<AristaFigura> tuneles) 
	{
		this.grafoDibujo.setDibujo(puntosDeInteres, tuneles);
	}
	
	public ArrayList<NodoFigura> getPuntosDeInteres()
	{
		return this.grafoDibujo.getPuntosDeInteres();
	}
	
	public ArrayList<AristaFigura> getTuneles()
	{
		return this.grafoDibujo.getTuneles();
	}
	
	public void habilitarAgregarPunto()
	{
		this.sePuedeAgregarPunto = true;
	}
	
	public void habilitarAgregarTunel()
	{
		this.sePuedeAgregarTunel = true;
	}
	
	public void deshabilitarAgregarPunto()
	{
		this.sePuedeAgregarPunto = false;
	}

	public void deshabilitarAgregarTunel()
	{
		this.sePuedeAgregarTunel = false;
	}
	@Override
	public void mouseEntered(MouseEvent e) 
	{
		
	}

	@Override
	public void mouseExited(MouseEvent e) 
	{
		
	}

	@Override
	public void mousePressed(MouseEvent e) 
	{
		
	}

	@Override
	public void mouseReleased(MouseEvent e) 
	{
		
	}
	
	@Override
	public void mouseDragged(MouseEvent e)
	{
		
	}

	@Override
	public void mouseMoved(MouseEvent e) 
	{
		if(this.sePuedeAgregarPunto)
		{
			this.grafoDibujo.resetearOrigenYDestinoTunel();
			this.punteroMouse.setLocation(e.getPoint());
			this.repaint();
		}
	}
	
	public void resetearOrigenYDestinoTunel()
	{
		this.grafoDibujo.resetearOrigenYDestinoTunel();
	}


//*****************************************************METODOS AUX
	
	private void pintarPunteroDeAgregarPunto(Graphics g)
	{
		if(this.sePuedeAgregarPunto)
		{
			g.setColor(Color.WHITE);
			g.fillOval((int)this.punteroMouse.getX() - NodoFigura.diametro/2, (int)this.punteroMouse.getY() - NodoFigura.diametro/2, NodoFigura.diametro, NodoFigura.diametro);
		}
	}
	
	private Point punteroMouse;
	
	private void pintarIndicadorDeAgregarTunel(Graphics g)
	{
		g.setColor(Color.WHITE);
		g.setFont(new Font("Agregar Tunel",Font.BOLD,20));
		g.drawString("Agregar Tunel", 5, 180);
		
		if(this.grafoDibujo.OcupacionPuntoOrigen())
		{
			g.setColor(Color.GREEN);
			g.setFont(new Font("Punto A",Font.BOLD,15));
			g.drawString("Punto A", 35, 210);
		}
		else
		{
			g.setColor(Color.WHITE);
			g.setFont(new Font("Punto A",Font.BOLD,15));
			g.drawString("Punto A", 35, 210);
		}
		if(this.grafoDibujo.OcupacionPuntoDestino())
		{
			g.setColor(Color.GREEN);
			g.setFont(new Font("Punto B",Font.BOLD,15));
			g.drawString("Punto B", 35,230);
		}
		else
		{
			g.setColor(Color.WHITE);
			g.setFont(new Font("Punto B",Font.BOLD,15));
			g.drawString("Punto B", 35, 230);
		}
	}

	public void setGrafoAux(Grafo grafo)
	{
		this.grafoDibujo.setGrafo(grafo);
	}

	public void setSolver(Solver nuevo)
	{
		this.solver = nuevo;	
	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
