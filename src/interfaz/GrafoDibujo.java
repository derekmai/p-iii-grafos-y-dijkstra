package interfaz;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

import grafo.Grafo;

public class GrafoDibujo 
{
	private ArrayList<NodoFigura> puntosInteres;
	private ArrayList<AristaFigura> tuneles;
	private Grafo grafoAux;
	private int cantSinNombre = 0;
	
	GrafoDibujo()
	{
		this.puntosInteres = new ArrayList<NodoFigura>();
		this.tuneles = new ArrayList<AristaFigura>();
		this.grafoAux = new Grafo(0);
	}

//***************************************************************METODOS PARA CREAR NODOS
	
	public void crearNodo(MouseEvent evento) throws IllegalArgumentException
	{
		int inputCarbon;
		String inputNombre = solicitarNombre();
		if(inputNombre!=null)
		{
			inputCarbon = solicitarReservaCarbon();
			if(inputCarbon!=-1)
			{
				
				NodoFigura aAgregar = new NodoFigura(inputNombre, inputCarbon, evento.getX(),evento.getY());
				this.grafoAux.agregarVertice(aAgregar.getNombre(),aAgregar.getReservaCarbon());
				this.puntosInteres.add(aAgregar);
			}
		}
	}
	
	private String solicitarNombre() 
	{
		String input = JOptionPane.showInputDialog("Ingrese nombre del punto de interes");
		if(input!=null)
		{
			if(input.equals(""))
			{
				cantSinNombre = dameCantidadDeNodosSinNombre();
				cantSinNombre++;
				return "sin nombre "+cantSinNombre;
			}
			return input;
		}
		else
			return null;
	}
	
	private int solicitarReservaCarbon() 
	{
		String input = null;
		boolean esValido = false;
		while(esValido == false)
		{
			input = JOptionPane.showInputDialog("Ingrese reserva de carbon en Toneladas (mayor a 0)");
			esValido = ChequearCarbon(input);
		}
		if(input!=null)
		{
			if(input.equals(""))
			{
				return 0;
			}
			return Integer.parseInt(input);
		}
		else
		{
			return -1;
		}
	}
	
	private boolean ChequearCarbon(String input)
	{
		if(input != null)
		{
			Pattern patron = Pattern.compile("\\d*");
			Matcher matcher = patron.matcher(input);
			
			if(matcher.matches())
			{
				return true;
			}
			return false;
		}
		return true;
	}
	
//**********************************************************************************METODOS PARA CREAR ARISTAS
	
	boolean chequearClickEnAreaNodo(NodoFigura nodo, MouseEvent evento) 
	{
		Rectangle areaDeNodo = new Rectangle(nodo.getX()-NodoFigura.diametro/2,  nodo.getY()-NodoFigura.diametro/2,  NodoFigura.diametro,  NodoFigura.diametro);
		if(areaDeNodo.contains(evento.getPoint()))
		{
			return true;
		}
		return false;
	}

	void fijarExtremosXY(NodoFigura nodo) 
	{
		if(origen == null)
		{
			origen = new NodoFigura(nodo.getNombre(),0,nodo.getX(), nodo.getY());
		}
		else
		{
			destino = new NodoFigura(nodo.getNombre(),0,nodo.getX(), nodo.getY());
			crearTunel();
		}
	}

	private void crearTunel() throws IllegalArgumentException
	{
		
		if(origen.getNombre().equals(destino.getNombre()) == false)
		{
			Integer longitud = solicitarLongitud();
			if(longitud >0)
			{
				grafoAux.agregarArista(grafoAux.getVertice(origen.getNombre()), grafoAux.getVertice(destino.getNombre()), longitud);
				tuneles.add(new AristaFigura(longitud, origen, destino));
				
			}
		}
		resetearOrigenYDestinoTunel();
	}

	private NodoFigura origen;
	private NodoFigura destino;

	private Integer solicitarLongitud() 
	{
		boolean esValido = false;
		Integer ret = null;
		String input = null;
		while(esValido == false)
		{
			input = JOptionPane.showInputDialog("Ingrese longitud de punto a punto en kms (mayor a 0)");
			esValido = chequearLongitud(input);
		}
		if(input == null)
			return -1;
		else if(input.equals(""))
			return 1;
		else
			ret = Integer.parseInt(input);
			return ret;
	}
	
	private boolean chequearLongitud(String input) 
	{
		if(input != null)
		{
			Pattern patron = Pattern.compile("[1-9]\\d*");
			Matcher matcher = patron.matcher(input);
			
			if(matcher.matches())
			{
				return true;
			}
			return false;
		}
		return true;
	}
	
	private int dameCantidadDeNodosSinNombre() 
	{
		int ret = 0;
		Pattern patron = Pattern.compile("sin nombre \\d*");
		for (NodoFigura nodo : puntosInteres)
		{
			Matcher matcher = patron.matcher(nodo.getNombre());
			
			if(matcher.matches())
			{
				ret++;
			}
		}
		return ret;
	}
	
//**********************************************************************************METODOS AUXILIARES Y PUBLICOS
	
	public boolean OcupacionPuntoOrigen()
	{
		if(this.origen == null)
		{
			return false;
		}
		return true;
	}
	
	public boolean OcupacionPuntoDestino()
	{
		if(this.destino == null)
		{
			return false;
		}
		return true;
	}
	
	public void setDibujo(ArrayList<NodoFigura> puntosDeInteres, ArrayList<AristaFigura> tuneles) 
	{
		if(puntosDeInteres != null)
		{
			this.puntosInteres = puntosDeInteres;
		}
		if(tuneles != null)
		{
			this.tuneles = tuneles;
		}
	}
	
	public ArrayList<NodoFigura> getPuntosDeInteres()
	{
		return this.puntosInteres;
	}
	
	public ArrayList<AristaFigura> getTuneles()
	{
		return this.tuneles;
	}

	public Grafo getGrafo() 
	{
		return this.grafoAux;
	}

	public void setGrafo(Grafo grafo) 
	{
		this.grafoAux = grafo;
	}
	public void resetearOrigenYDestinoTunel()
	{
		origen = null;
		destino = null;
	}
}
