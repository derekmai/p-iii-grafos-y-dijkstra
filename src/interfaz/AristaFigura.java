package interfaz;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.io.Serializable;

public class AristaFigura implements Serializable
{
	private static final long serialVersionUID = 1L;
	private int x1, y1, x2, y2;
	private Integer longitud;
	private String nombreOrigen;
	private String nombreDestino;

	public AristaFigura(Integer longitud, NodoFigura origen, NodoFigura destino) 
	{
		nombreOrigen = origen.getNombre();
		setNombreDestino(destino.getNombre());
		this.x1 = destino.getX();
		this.y1 = destino.getY();
		this.x2 = origen.getX();
		this.y2 =  origen.getY();
		this.longitud = longitud;
	}
	public void pintar(Graphics g)
	{
		g.setColor(Color.WHITE);
		g.drawLine(x1, y1, x2, y2);
		this.pintarDistancia(g);
	}

	public void pintarCamino(Graphics g)
	{
		g.setColor(Color.GREEN);
		g.drawLine(x1, y1, x2, y2);
		this.pintarDistancia(g);
	}
	
	private void pintarDistancia(Graphics g) 
	{
		Point punto = this.calcularUbicacion();
		g.setColor(Color.BLACK);
		g.setFont(new Font(this.longitud+" km", Font.TYPE1_FONT, 15));
		int x = (int) punto.getX();
		int y = (int) punto.getY();
		g.drawString(this.longitud+"km", x, y);
	}

	private Point calcularUbicacion() 
	{
		Point ret = null;
		if(x1 > x2 && y1 > y2)
		{
			ret = new Point(x1 - Math.abs(x1 - x2)/2, y1 - Math.abs(y1 - y2)/2);
			return ret;
		}
		if(x1 < x2 && y1 < y2)
		{
			ret = new Point(x2 - Math.abs(x1 - x2)/2, y2 - Math.abs(y1 - y2)/2);
			return ret;
		}
		if(x1 > x2 && y1 < y2)
		{
			ret = new Point(x1 - Math.abs(x1 - x2)/2, y2 - Math.abs(y1 - y2)/2);
			return ret;
		}
		if(x1 < x2 && y1 > y2)
		{
			ret = new Point(x2 - Math.abs(x1 - x2)/2, y1 - Math.abs(y1 - y2)/2);
			return ret;
		}
		if(x1 == x2 && y1 == y2)
		{
			ret = new Point(x1, y1);
		}
		return ret;
	}

	public int getX1() 
	{
		return x1;
	}

	public void setX1(int x1) 
	{
		this.x1 = x1;
	}

	public int getY1() 
	{
		return y1;
	}

	public void setY1(int y1) 
	{
		this.y1 = y1;
	}

	public int getX2() 
	{
		return x2;
	}

	public void setX2(int x2) 
	{
		this.x2 = x2;
	}

	public int getY2() 
	{
		return y2;
	}

	public void setY2(int y2) 
	{
		this.y2 = y2;
	}
	public String getNombreOrigen()
	{
		return this.nombreOrigen;
	}
	public String getNombreDestino()
	{
		return nombreDestino;
	}
	public void setNombreDestino(String nombreDestino) {
		this.nombreDestino = nombreDestino;
	}
}
