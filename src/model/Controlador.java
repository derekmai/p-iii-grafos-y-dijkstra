package model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import datos.Datos;
import grafo.Grafo;
import interfaz.AristaFigura;
import interfaz.MainApp;

public class Controlador implements ActionListener
{
	private Grafo grafo;
	private MainApp ventana;
	private Datos datos;
	
	public Controlador() throws FileNotFoundException, IOException
	{
		this.datos = new Datos();
	}
	
	public void setVentana(MainApp ventana)
	{
		this.ventana = ventana;
	}

	//********************************************************EVENTOS DE BOTONES
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getActionCommand().equalsIgnoreCase("crear mapa"))
		{
			this.ventana.crearPanelNuevoMapa();
		}
		if(e.getActionCommand().equalsIgnoreCase("crear"))
		{
			this.grafo = new Grafo(0);
			try 
			{
				this.datos.guardarDatos(this.ventana.getNombreMapaNuevo(), grafo, null, null);
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}
			this.ventana.crearPanelInicio();
		}
		if(e.getActionCommand().equalsIgnoreCase("cargar mapa"))
		{
			String[] lista = this.datos.getMapas();
			this.ventana.crearPanelCarga(lista);
		}
		if(e.getActionCommand().equalsIgnoreCase("cargar"))
		{
			if(this.ventana.getMapaSeleccionado() != -1)
			{
				String mapaElegido = this.datos.getMapas()[this.ventana.getMapaSeleccionado()];
				this.datos.cargarDatos(mapaElegido);
				this.ventana.crearPanelVisualizacion();
				this.ventana.setFiguras(this.datos.getNodosFigura(),this.datos.getAristasFigura());
				this.ventana.setGrafoAux(this.datos.getGrafo());
			}
			else
			{
				this.ventana.crearPanelInicio();
			}
		}
		if(e.getActionCommand().equalsIgnoreCase("volver al inicio"))
		{
			this.ventana.crearPanelInicio();
		}
		if(e.getActionCommand().equalsIgnoreCase("guardar"))
		{

			handleEstadoDePuntoTunelYCaminoMinimo();
			this.ventana.setSolver(null);
			this.grafo = this.ventana.getGrafoArmado();
			
			String mapaElegido = this.datos.getMapas()[this.ventana.getMapaSeleccionado()];
			ArrayList<AristaFigura> aristas = this.ventana.getAristasFigura();
			try 
			{
				this.datos.guardarDatos(mapaElegido, grafo, this.ventana.getNodosFigura(), this.ventana.getAristasFigura());
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}
			if (aristas.size()>0)
				this.ventana.botonCaminoMinimo(true);
			if (this.grafo.getCantidadVertices()>0)
				this.ventana.botonCrearTunel(true);
		}
		if(e.getActionCommand().equalsIgnoreCase("aplicar camino minimo"))//FALTA EL METODO DE BUSCAR EL CAMINO MINIMO
		{
			
			handleEstadoDePuntoTunelYCaminoMinimo();
			this.ventana.setSolver(null);
			this.ventana.habilitarSePuedeCaminoMinimo();
		}
		if(e.getActionCommand().equalsIgnoreCase("agregar Punto"))
		{
			this.ventana.habilitarAgregarPunto();
			this.ventana.deshabilitarSePuedeCaminoMinimo();
			this.ventana.setSolver(null);		
		}
		if(e.getActionCommand().equalsIgnoreCase("agregar tunel"))
		{
			//preguntar si cant de nodos > 1 para habilitar agregar tunel.
			if (this.ventana.getGrafoArmado().getCantidadVertices()<=1)
			{
				JOptionPane.showMessageDialog(this.ventana,"No se puede agregar tuneles porque hay un solo vertice!");
				return;
			}
			this.ventana.habilitarAgregarTunel();
			this.ventana.deshabilitarAgregarPunto();
			this.ventana.deshabilitarSePuedeCaminoMinimo();
			this.ventana.setSolver(null);
		}
		this.ventana.repaint();
	}

	private void handleEstadoDePuntoTunelYCaminoMinimo()
	{
		if (this.ventana.estaHabilitadoAgregarPunto())
			this.ventana.deshabilitarAgregarPunto();
		if (this.ventana.estaHabilitadoAgregarTunel())
		{
			this.ventana.deshabilitarAgregarTunel();
			this.ventana.resetearOrigenYDestinoTunel();
		}
		if (this.ventana.estaHabilitadoCaminoMinimo())
			this.ventana.deshabilitarSePuedeCaminoMinimo();
	}

	public int getCantidadNodos()
	{
		return this.grafo.getCantidadVertices();
	}
	
	
}
