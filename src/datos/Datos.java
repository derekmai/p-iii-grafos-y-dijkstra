package datos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import grafo.Grafo;
import interfaz.AristaFigura;
import interfaz.NodoFigura;

public class Datos 
{
	private Grafo grafo;
	private ArrayList<String> mapas;
	private ArrayList<NodoFigura> nodosFig;
	private ArrayList<AristaFigura> aristasFig;

	public Datos() throws FileNotFoundException, IOException
	{
		this.mapas = cargarMapas("src/datos/mapas.txt");
	}
	
	public String[] getMapas()
	{
		String[] ret = new String[this.mapas.size()];
		int indice = 0;
		for(String mapa : this.mapas)
		{
			ret[indice] = mapa;
			indice++;
		}
		return ret;
	}
	
	public Grafo getGrafo()
	{
		return this.grafo;
	}
	
	public ArrayList<NodoFigura> getNodosFigura()
	{
		return this.nodosFig;
	}
	
	public ArrayList<AristaFigura> getAristasFigura()
	{
		return this.aristasFig;
	}
	
//******************************************************CARGA DE MAPAS
	
	private static ArrayList<String> cargarMapas(String ruta) throws FileNotFoundException, IOException
	{
		ArrayList<String> a = new ArrayList<String>();
		String cadena;
	    FileReader f = new FileReader(ruta);
	    BufferedReader b = new BufferedReader(f);
	    
	    while((cadena = b.readLine())!=null)
	    {
	    	a.add(cadena);
	    }
	    b.close();
	    return a;
	}
	
//*******************************************************METODOS PARA CARGAR DATOS
	
	public void cargarDatos(String nombreMapa)
	{
		this.cargarGrafo(nombreMapa);
		this.cargarFiguras(nombreMapa);
	}

	private void cargarGrafo(String nombreMapa) 
	{
		try 
		{
			ObjectInputStream recuperando_datos = new ObjectInputStream(new FileInputStream("src/datos/"+nombreMapa+"/grafo.dat"));		
			this.grafo = (Grafo) recuperando_datos.readObject();
			recuperando_datos.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void cargarFiguras(String nombreMapa)
	{
		try 
		{
			ObjectInputStream recuperando_datos = new ObjectInputStream(new FileInputStream("src/datos/"+nombreMapa+"/nodosFiguras.dat"));		
			this.nodosFig = (ArrayList<NodoFigura>) recuperando_datos.readObject();
			recuperando_datos.close();
			
			ObjectInputStream recuperando_datos2 = new ObjectInputStream(new FileInputStream("src/datos/"+nombreMapa+"/aristasFiguras.dat"));		
			this.aristasFig = (ArrayList<AristaFigura>) recuperando_datos2.readObject();
			recuperando_datos2.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

//*******************************************************METODOS PARA GUARDAR DATOS
	
	public void guardarDatos(String nombreMapa, Grafo grafo, ArrayList<NodoFigura> nodosFig, ArrayList<AristaFigura> aristasFig) throws IOException
	{
		nombreMapa = nombreMapa.toUpperCase();
		crearCarpeta("src/datos/"+nombreMapa);
		this.agregarMapa(nombreMapa);
		this.guardarMapas("src/datos/mapas.txt",this.mapas);
		this.guardarGrafo(nombreMapa,grafo);
		this.guardarFiguras(nombreMapa,nodosFig,aristasFig);
	}
	
	private void guardarMapas(String ruta,ArrayList<String> mapas) throws IOException	//SOBREESCRIBE LOS MAPAS
	{
		File archivo = new File(ruta);
		BufferedWriter bw;
		if(archivo.exists())
		{
			bw = new BufferedWriter(new FileWriter(archivo));
			for(String cadena : mapas)
			{
				bw.write(cadena);
				bw.newLine();
			}
			bw.close();
		}
		else
		{
			System.out.println("EL TXT DE MAPAS NO EXITE");
		}
	}
	
	private void agregarMapa(String nombreMapa)
	{
		if(this.mapas.contains(nombreMapa) == false)
		{
			this.mapas.add(nombreMapa);
		}
	}

	private void guardarGrafo(String nombreMapa, Grafo grafo)
	{
		try
		{
			ObjectOutputStream escribiendo_fichero = new ObjectOutputStream(new FileOutputStream("src/datos/"+nombreMapa+"/grafo.dat"));
			escribiendo_fichero.writeObject(grafo);
			escribiendo_fichero.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void guardarFiguras(String nombreMapa, ArrayList<NodoFigura> nodosFig, ArrayList<AristaFigura> aristasFig) 
	{
		try
		{
			ObjectOutputStream escribiendo_fichero = new ObjectOutputStream(new FileOutputStream("src/datos/"+nombreMapa+"/nodosFiguras.dat"));
			escribiendo_fichero.writeObject(nodosFig);
			escribiendo_fichero.close();
			
			ObjectOutputStream escribiendo_fichero2 = new ObjectOutputStream(new FileOutputStream("src/datos/"+nombreMapa+"/aristasFiguras.dat"));
			escribiendo_fichero2.writeObject(aristasFig);
			escribiendo_fichero2.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
//********************************************************************************************************	METODOS AUXILIARES
	
	private void crearCarpeta(String ruta) 
	{
		File carpeta = new File(ruta);
		carpeta.mkdir();
	}
	
}